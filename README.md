# BASH scripts 

These scripts can be copied to your home directory, or symlinked from a cloned repository. A simple sym-linking script is provided.

```
cd
git clone https://jgorman@bitbucket.org/jgorman/bash.git
bash/INSTALL
```

[`prepare-commit-msg-jira`](https://bitbucket.org/jgorman/prepare-commit-msg-jira) is recommended if using git with JIRA.

These are currently tested on OS X Yosemite. Your milage may vary.

## Copyright

`.git-prompt.sh` and `.git-completion.bash` are (C) Shawn O. Pearce.

`.colour.bash` is (C) Media Done Right. 

All other source is (C) James Gorman.

All source is release under [GPLv2](http://www.gnu.org/licenses/gpl-2.0.html) except `.colour.bash` which is free to use.
