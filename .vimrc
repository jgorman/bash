" Default vim settings

" Copyright (C) 2014 James Gorman <jamesgorman2@gmail.com>
" Distributed under the GNU General Public License, version 2.0.
" http://www.gnu.org/licenses/gpl-2.0.html

syntax on
