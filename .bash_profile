# BASH settings and commands

# Copyright (C) 2014 James Gorman <jamesgorman2@gmail.com>
# Distributed under the GNU General Public License, version 2.0.
# http://www.gnu.org/licenses/gpl-2.0.html

## general terminal settings

export CLICOLOR=1

alias ls='ls -G'
alias ll="ls -lah"

SHARE=/usr/local/share

## JVM

export JAVA_HOME=$(/usr/libexec/java_home)
export SCALA_HOME=${SHARE}/scala-2.11.4
export GRADLE_HOME=${SHARE}/gradle-2.2.1

export MAVEN_OPTS="-Xmx512m"

## git stuff

# setup prepare-commit-msg-jira if installed
which clone &> /dev/null
RES=$?
if [ "$RES" = "0" ]; then
  git() {
    if [[ $1 == "clone" ]]; then
      shift
      command clone "$@"
    else
      command git "$@"
    fi 
  }
fi

if [ -f ~/.git-completion.bash ]; then
  . ~/.git-completion.bash
fi

## terminal prompt
# git prompt
if [ -f ~/.git-prompt.sh ]; then
  . ~/.git-prompt.sh

  export GIT_PS1_SHOWDIRTYSTATE=1
  export GIT_PS1_SHOWSTASHSTATE=1
  export GIT_PS1_SHOWUNTRACKEDFILES=1
  export GIT_PS1_SHOWUPSTREAM="auto"
  export GIT_PS1_SHOWCOLORHINTS=1
  PROMPT_COMMAND='__git_ps1 "" "$Blue\u@\h:$Green\w$White\$ " "[%s]\n"'
else
  . ~/.colour.bash
  PS1="$Blue\u@\h:$Green\w$White\$ "
fi

## path

# git
export PATH=/usr/local/git/bin:$PATH

# python
# Setting PATH for Python 3.4
PY_HOME="/Library/Frameworks/Python.framework/Versions/3.4/bin"
export PATH=$PY_HOME:$PATH

# scala
export PATH=~/bin:$SCALA_HOME/bin:$GRADLE_HOME/bin:$PATH

# c
which pkg-config &> /dev/null
RES=$?
if [ "$RES" = "0" ]; then
  export PATH=/usr/local/opt/gettext/bin:$PATH
  export C_INCLUDE_PATH=/usr/local/include:$C_INCLUDE_PATH
  export LIBRARY_PATH=~/local/lib:/usr/local/lib:/usr/local/opt/readline/lib:$LIBRARY_PATH
  export LIBXML2_LIBS=`pkg-config --libs libxml-2.0`
  export LIBXML2_CFLAGS=`pkg-config --cflags libxml-2.0`
fi

# node
ls $(brew --prefix nvm)/nvm.sh &> /dev/null
RES=$?
if [ "$RES" = "0" ]; then  
  export NVM_DIR=~/.nvm
  source $(brew --prefix nvm)/nvm.sh
fi

# postgres
export PATH=/Library/PostgreSQL/9.4/bin/:$PATH

# docker
which docker-machine &> /dev/null
RES=$?
if [ "$RES" = "0" ]; then
  eval "$(docker-machine env default)"
fi
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
